# FileReceiver

API para receber contratos em PDF, persistir no S3 e notificar no SQS

# Itens para melhoria
* Colocar segurança na API (API Gateway corporativa).
* Colocar as configurações da AWS no appsettings.
* Usar IOptions para passar a configuração do AWS
* Criar melhor gerenciamento e controle do uso dos serviços S3 e SQS
* Alterar forma de receber o usuário e o contrato 
* Apagar arquivo do bucket quando houver erro de comunicação com o SQS
* Inclusão dos logs