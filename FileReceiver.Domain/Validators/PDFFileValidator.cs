﻿using FileReceiver.Domain.Entities;
using FluentValidation;

namespace FileReceiver.Domain.Validators
{
    public class PDFFileValidator : AbstractValidator<PDFFile>
    {
        public PDFFileValidator()
        {
            RuleFor(a => a.Stream.Length)
                .NotNull()
                .GreaterThan(0)
                .WithMessage("Arquivo carregado de forma incorreta.");

            RuleFor(a => a.UserId)
                .NotEmpty()
                .WithMessage("Usuário não pode ser vazio.")
                .NotNull()
                .WithMessage("Usuário não pode ser nulo.");

            RuleFor(a => a.ContentType)
                .NotNull()
                .Must(x => x.Equals("application/pdf"))
                .WithMessage("Extensão de arquivo inválida.");

            RuleFor(r => r.ContractType)
                .NotEmpty()
                .WithMessage("Tipo de contrato não pode ser vazio")
                .NotNull()
                .WithMessage("Tipo de contrato não pode ser nulo");
        }
    }
}
