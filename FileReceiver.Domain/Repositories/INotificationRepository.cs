﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReceiver.Domain.Repositories
{
    public interface INotificationRepository
    {
        Task<bool> Send(string notification);
    }
}
