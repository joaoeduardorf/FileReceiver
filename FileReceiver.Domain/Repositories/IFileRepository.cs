﻿using FileReceiver.Domain.Entities;
using System.Threading.Tasks;

namespace FileReceiver.Domain.Repositories
{
    public interface IFileRepository
    {
        public Task<bool> SaveAsync(PDFFile pdfFile);
    }
}
