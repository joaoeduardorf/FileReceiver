﻿using FileReceiver.Domain.Validators;
using System;
using System.IO;

namespace FileReceiver.Domain.Entities
{
    public class PDFFile : Entity
    {
        private const string SEPARATOR = "-";
        public PDFFile(MemoryStream stream, string userId, string contractType, string contentType)
        {
            Stream = stream;
            UserId = userId;
            ContractType = contractType;
            ContentType = contentType;

            Validate(this, new PDFFileValidator());

        }

        public MemoryStream Stream { get; set; }
        public string ContractType { get; set; }
        public string ContentType { get; set; }
        public string UserId { get; set; }

        public string Name
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-dd-HH-mm-ss") + SEPARATOR + UserId + ".pdf";
            }

        }
    }
}
