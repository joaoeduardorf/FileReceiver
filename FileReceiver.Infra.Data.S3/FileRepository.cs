﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using FileReceiver.Domain.Entities;
using FileReceiver.Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace FileReceiver.Infra.Data.S3
{
    public class FileRepository : IFileRepository
    {
        private readonly IAmazonS3 _s3Client;

        private const string bucketName = "desafio-itau";

        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.SAEast1;

        public FileRepository()
        {
            _s3Client = new AmazonS3Client("AWSKeyId", "AWSSecretAccessKey", bucketRegion);
        }

        public async Task<bool> SaveAsync(PDFFile pdfFile)
        {
            bool result = false;
            try
            {
                var fileTransferUtility = new TransferUtility(_s3Client);

                await fileTransferUtility.UploadAsync(pdfFile.Stream, bucketName, pdfFile.ContractType + "/" + pdfFile.Name);

                result = true;

            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }


            return result;
        }
    }
}
