﻿using FileReceiver.AppService.Requests;
using FileReceiver.Domain.Entities;

namespace FileReceiver.AppService.Mappers
{
    public static class PDFFileMapper
    {
        public static PDFFile Mapper(this SaveFileRequest pdfFileRequest)
        {
            return new PDFFile(pdfFileRequest.Stream, pdfFileRequest.UserId, pdfFileRequest.ContractType, pdfFileRequest.ContentType);
        }
    }
}
