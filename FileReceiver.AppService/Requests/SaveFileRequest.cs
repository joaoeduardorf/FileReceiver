﻿using MediatR;
using System.IO;

namespace FileReceiver.AppService.Requests
{
    public class SaveFileRequest : IRequest<bool>
    {
        public MemoryStream Stream { get; set; }
        public string ContractType { get; set; }
        public string ContentType { get; set; }
        public string UserId { get; set; }

        public SaveFileRequest(MemoryStream stream, string userId, string contractType, string contentType)
        {
            Stream = stream;
            UserId = userId;
            ContractType = contractType;
            ContentType = contentType;
        }
    }
}
