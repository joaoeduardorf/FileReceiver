﻿using FileReceiver.AppService.Mappers;
using FileReceiver.AppService.Requests;
using FileReceiver.Core.Notifications;
using FileReceiver.Domain.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FileReceiver.AppService.Handlers
{
    public class SavePDFFileHandler : IRequestHandler<SaveFileRequest, bool>
    {
        private readonly NotificationContext _notificationContext;
        private readonly IFileRepository _fileRepository;
        private readonly INotificationRepository _notificationRepository;

        public SavePDFFileHandler(NotificationContext notificationContext, IFileRepository fileRepository, INotificationRepository notificationRepository)
        {
            _notificationContext = notificationContext;
            _fileRepository = fileRepository;
            _notificationRepository = notificationRepository;

        }

        public async Task<bool> Handle(SaveFileRequest request, CancellationToken cancellationToken)
        {
            var pdfFile = request.Mapper();

            if (pdfFile.Invalid)
            {
                _notificationContext.AddNotifications(pdfFile.ValidationResult);
                return pdfFile.Valid;
            }

            bool result = await _fileRepository.SaveAsync(pdfFile);

            if (!result)
            {
                _notificationContext.AddNotification(new Notification("SalvarContrato", "Falha ao salvar contrato"));
                return result;
            }
            else
            {
                result = await _notificationRepository.Send(pdfFile.Name);
            }

            if (!result)
            {
                _notificationContext.AddNotification(new Notification("NotificarProcessamento", "Falha na notificação para o processamento do arquivo"));
            }
            


            return result;
        }
    }
}
