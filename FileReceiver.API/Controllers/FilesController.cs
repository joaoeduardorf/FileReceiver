﻿using FileReceiver.AppService.Requests;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FileReceiver.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IMediator _mediator;
        public FilesController(IMediator mediator)
        {
            _mediator = mediator;
        }
        // POST api/<FilesController>
        [HttpPost]
        public async Task PostAsync(IFormFile file, string userId, string contractType)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                _ = file.CopyToAsync(memoryStream);

                await _mediator.Send(new SaveFileRequest(memoryStream, userId, contractType, file.ContentType));

            }
        }
    }
}
