﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileReceiver.API.Requests
{
    public class AditionalInformationsRequests
    {
        public string UserId { get; set; }
        public string ContractType { get; set; }
    }
}
