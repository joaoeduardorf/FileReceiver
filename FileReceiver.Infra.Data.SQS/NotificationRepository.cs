﻿using Amazon.SQS;
using Amazon.SQS.Model;
using FileReceiver.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FileReceiver.Infra.Data.SQS
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly AmazonSQSClient _amazonSQSClient;
        public NotificationRepository()
        {
            AmazonSQSConfig amazonSQSConfig = new AmazonSQSConfig
            {
                ServiceURL = "https://sqs.sa-east-1.amazonaws.com"
            };

            _amazonSQSClient = new AmazonSQSClient("AWSKeyId", "AWSSecretAccessKey", amazonSQSConfig);
        }
        public async Task<bool> Send(string notification)
        {
            bool result = false;

            var sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = "https://sqs.sa-east-1.amazonaws.com/817195461708/desafio-itau",
                MessageBody = notification,
            };
            try
            {
                var response = await _amazonSQSClient.SendMessageAsync(sendMessageRequest);
                result = true;
            }
            catch (AmazonSQSException sqse)
            {
                Console.WriteLine(sqse.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }



            return result;
        }
    }
}